package com.example.core.retrofit

import com.example.core.model.Characters
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RepositoryService {

    @GET("character/")
    fun getCharacters(@Query(QUERY_PAGE) page: Int = startedPage): Single<Characters>

    companion object {
        private const val QUERY_PAGE: String = "page"
        val startedPage = 1
    }

}