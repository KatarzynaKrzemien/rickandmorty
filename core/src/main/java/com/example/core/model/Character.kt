package com.example.core.model

import com.google.gson.annotations.SerializedName

class Characters(
    val info: Info = Info(),
    val results: List<CharacterResult> = emptyList()
)

data class CharacterResult(
    override val id: Int = Item.defaultId(),
    val name: String = String(),
    val status: String = String(),
    val species: String = String(),
    val type: String = String(),
    val gender: String = String(),
    val origin: Origin,
    val location: Location,
    @SerializedName("image")
    val imageUrl: String = String(),
    val episode: List<String> = emptyList(),
    val url: String = String(),
    val created: String = String()
) : Item

data class Origin(
    val name: String = String(),
    val url: String = String()
)

data class Location(
    val name: String = String(),
    val url: String = String()
)