package com.example.core.model

data class Info(
    val count: Int = 0,
    val pages: Int = 0,
    val next: String = String(),
    val prev: String = String()
)