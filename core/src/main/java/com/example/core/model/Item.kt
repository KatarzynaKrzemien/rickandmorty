package com.example.core.model

import java.util.*

interface Item {
    val id: Int

    companion object {
        fun defaultId() = UUID.randomUUID().hashCode()
    }
}