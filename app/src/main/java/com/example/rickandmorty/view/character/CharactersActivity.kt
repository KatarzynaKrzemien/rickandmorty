package com.example.rickandmorty.view.character

import com.example.domain.viewmodel.character.CharactersViewModel
import com.example.rickandmorty.R
import com.example.rickandmorty.databinding.ActivityCharactersBinding
import com.example.rickandmorty.view.BaseActivity

class CharactersActivity : BaseActivity<ActivityCharactersBinding, CharactersViewModel>() {

    override val layoutRes: Int = R.layout.activity_characters
    override val viewModelJavaClass: Class<CharactersViewModel> = CharactersViewModel::class.java
}
