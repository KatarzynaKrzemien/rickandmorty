package com.example.rickandmorty.view.character.details

import com.example.domain.viewmodel.character.details.CharacterDetailsViewModel
import com.example.rickandmorty.R
import com.example.rickandmorty.databinding.ActivityCharacterDetailsBinding
import com.example.rickandmorty.view.BaseActivity

class CharacterDetailsActivity :
    BaseActivity<ActivityCharacterDetailsBinding, CharacterDetailsViewModel>() {

    override val layoutRes: Int = R.layout.activity_character_details
    override val viewModelJavaClass: Class<CharacterDetailsViewModel> =
        CharacterDetailsViewModel::class.java
}
