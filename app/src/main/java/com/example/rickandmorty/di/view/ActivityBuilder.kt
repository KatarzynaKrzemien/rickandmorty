package com.example.rickandmorty.di.view

import com.example.rickandmorty.di.CharacterDetailsScope
import com.example.rickandmorty.di.CharactersScope
import com.example.rickandmorty.di.view.character.CharactersModule
import com.example.rickandmorty.di.view.character.details.CharacterDetailsModule
import com.example.rickandmorty.view.character.CharactersActivity
import com.example.rickandmorty.view.character.details.CharacterDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [CharactersModule::class])
    @CharactersScope
    abstract fun charactersActivity(): CharactersActivity

    @ContributesAndroidInjector(modules = [CharacterDetailsModule::class])
    @CharacterDetailsScope
    abstract fun charactersDetailsActivity(): CharacterDetailsActivity

}
