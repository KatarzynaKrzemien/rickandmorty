package com.example.rickandmorty.di

import javax.inject.Scope

@Retention
@Scope
annotation class CharacterDetailsScope

@Retention
@Scope
annotation class CharactersScope
