package com.example.rickandmorty.di.viewmodel

import com.example.core.retrofit.RepositoryService
import com.example.core.rx.RxSchedulers
import com.example.domain.viewmodel.ViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ViewModelFactoryModule {

    @Provides
    fun provideViewModelFactoryModule(
        repositoryService: RepositoryService,
        rxSchedulers: RxSchedulers
    ): ViewModelFactory =
        ViewModelFactory(
            repositoryService,
            rxSchedulers
        )
}
