package com.example.rickandmorty.di

import android.content.Context
import com.example.rickandmorty.App
import com.example.rickandmorty.di.retrofit.RetrofitModule
import com.example.rickandmorty.di.rx.RxModule
import com.example.rickandmorty.di.view.ActivityBuilder
import com.example.rickandmorty.di.viewmodel.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilder::class,
        RxModule::class,
        RetrofitModule::class,
        ViewModelFactoryModule::class
    ]
)

@Singleton
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(app: Context): Builder

        fun build(): AppComponent
    }
}
