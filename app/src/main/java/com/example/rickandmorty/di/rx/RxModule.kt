package com.example.rickandmorty.di.rx

import com.example.core.rx.RxSchedulers
import com.example.domain.rx.AndroidRxSchedulers
import dagger.Module
import dagger.Provides

@Module
class RxModule {
    @Provides
    fun provideRxSchedulers(): RxSchedulers = AndroidRxSchedulers()
}