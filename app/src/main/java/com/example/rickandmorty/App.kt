package com.example.rickandmorty

import android.app.Application
import com.example.rickandmorty.di.AppComponent
import com.example.rickandmorty.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject


class App : Application(), HasAndroidInjector
{
    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Any>

    private val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appContext(this).build()
    }

    override fun onCreate() {
        appComponent.inject(this)
        super.onCreate()
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingActivityInjector
}