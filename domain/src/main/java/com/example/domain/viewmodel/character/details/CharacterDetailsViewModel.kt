package com.example.domain.viewmodel.character.details

import com.example.core.retrofit.RepositoryService
import com.example.core.rx.RxSchedulers
import com.example.domain.viewmodel.BaseViewModel

class CharacterDetailsViewModel(repository: RepositoryService, rxSchedulers: RxSchedulers) :
    BaseViewModel(repository, rxSchedulers)
