package com.example.domain.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.core.retrofit.RepositoryService
import com.example.core.rx.RxSchedulers
import com.example.domain.viewmodel.character.CharactersViewModel
import com.example.domain.viewmodel.character.details.CharacterDetailsViewModel

class ViewModelFactory(
    var repository: RepositoryService,
    var rxSchedulers: RxSchedulers
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(CharactersViewModel::class.java) ->
            CharactersViewModel(
                repository,
                rxSchedulers
            ) as T

        modelClass.isAssignableFrom(CharacterDetailsViewModel::class.java) ->
            CharacterDetailsViewModel(repository, rxSchedulers) as T

        else -> throw IllegalArgumentException("ViewModel Not Found")
    }
}
