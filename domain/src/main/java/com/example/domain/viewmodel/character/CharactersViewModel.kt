package com.example.domain.viewmodel.character

import com.example.core.retrofit.RepositoryService
import com.example.core.rx.RxSchedulers
import com.example.domain.viewmodel.BaseViewModel

class CharactersViewModel(repository: RepositoryService, rxSchedulers: RxSchedulers) :
    BaseViewModel(repository, rxSchedulers)
