package com.example.domain.viewmodel

import androidx.lifecycle.ViewModel
import com.example.core.retrofit.RepositoryService
import com.example.core.rx.RxSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BaseViewModel(
    protected val repository: RepositoryService,
    protected val rxSchedulers: RxSchedulers
) : ViewModel() {

    private val disposables = CompositeDisposable()

    protected fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}